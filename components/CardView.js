import React,{useEffect,useState} from 'react';
import { withNavigation } from '@react-navigation/compat';
import PropTypes from 'prop-types';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme } from 'galio-framework';

import { argonTheme } from '../constants';
import axios from 'axios'
import {baseWeather,option} from '../api/config'
const CardView = (props) => {
    const { navigation, item, horizontal, full, style, ctaColor, imageStyle } = props;
    
    const imageStyles = [
      full ? styles.fullImage : styles.horizontalImage,
      imageStyle
    ];
    const cardContainer = [styles.card, styles.shadow, style];
    const imgContainer = [styles.imageContainer,
      horizontal ? styles.horizontalStyles : styles.verticalStyles,
      styles.shadow
    ];
    const [arrObj, setArrObj] = useState({})
    useEffect(() => {
      getApi()
    },[])
    const handleSwapNavigation = (name) => {
      return navigation.navigate(name)
    }
    const getApi = async() => {
      try {
        const result = await axios.get(`${baseWeather}/${item.woeid}`,{
          headers:option
        })
        if(result.data) {
          setArrObj(result?.data?.consolidated_weather[0] ?result?.data?.consolidated_weather[0] : {} )
        }
      } catch (error) {
        setData({})
      }
    }
    const fixNumber =(name) => name.toFixed(2)
  return(
      <Block row={horizontal} card flex style={cardContainer}>
        <TouchableWithoutFeedback >
          <Block flex style={imgContainer}>
            <Image source={{uri: item.image}} style={imageStyles} />
          </Block>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback >
          <Block flex space="between" style={styles.cardDescription}>
            <Text size={14} style={styles.cardTitle}>{item.title}</Text>
            <Text size={14} style={styles.cardTitle}>Nhiệt độ nhỏ nhất: {arrObj.min_temp ? fixNumber(arrObj.min_temp+32-23) +`℃` : ''}</Text>
            <Text size={14} style={styles.cardTitle}>Nhiệt độ cao nhất: {arrObj.max_temp ? fixNumber(arrObj.max_temp+32-23) +`℃`: ''}</Text>
            <Text size={14} style={styles.cardTitle}>Độ ẩm: {arrObj.humidity ? fixNumber(arrObj.humidity) : ''}</Text>
            <Text size={14} style={styles.cardTitle}>Sức gió: {arrObj.wind_speed ? fixNumber(arrObj.wind_speed): ''}</Text>
          </Block>
        </TouchableWithoutFeedback>
      </Block>
  )
} 


CardView.propTypes = {
  item: PropTypes.object,
  horizontal: PropTypes.bool,
  full: PropTypes.bool,
  ctaColor: PropTypes.string,
  imageStyle: PropTypes.any,
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 114,
    marginBottom: 16
  },
  cardTitle: {
    flex: 1,
    flexWrap: 'wrap',
    paddingBottom: 6
  },
  cardDescription: {
    padding: theme.SIZES.BASE / 2
  },
  imageContainer: {
    borderRadius: 3,
    elevation: 1,
    overflow: 'hidden',
  },
  image: {
    // borderRadius: 3,
  },
  horizontalImage: {
    height: 122,
    width: 'auto',
  },
  horizontalStyles: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  verticalStyles: {
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  },
  fullImage: {
    height: 215
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },
});

export default CardView;