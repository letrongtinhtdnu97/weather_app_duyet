import React from 'react';
import {View, Text, StyleSheet,Dimensions} from 'react-native'
import Icon from './Icon';
const Header = (props) => {
  const {left,onPressLeft,title} = props
  return(
    <View style={styles.container}>
      {
        left ? <Icon
        onPress={onPressLeft}
        name="chevron-left"
        family="entypo"
        size={30}
      /> : <View></View>
      }
      <Text style={{fontSize:21}}>{title ? title : ''}</Text>
      <Text></Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    paddingHorizontal:10,
    height:50, 
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    borderBottomWidth:0.5,
    borderBottomColor:'gray'
  }
})
export default Header;