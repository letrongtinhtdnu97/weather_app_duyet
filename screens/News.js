import React, { useEffect } from 'react';
import {View, Text,Linking} from 'react-native'
function News(props) {
    const {route} = props
    const {url} = route.params
    console.log(url)
    useEffect(() => {
        Linking.openURL(url)
        .then(() => {
            return props.navigation.goBack()
        }).catch((err) => console.error('An error occurred', err))
    },[url])
    return (
        <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <Text>Loading....</Text>
        </View>
    );
}

export default News;