import React from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Block, theme } from 'galio-framework';

import { CardView,Card } from '../components';
import articles from '../constants/articles';
const { width } = Dimensions.get('screen');
import Header from '../components/Header';
import axios from 'axios'
import {baseURL,option} from '../api/config'
const data = [
    {
      id: 1,
      title: 'Hồ Chí Minh',
      image: 'https://images.unsplash.com/photo-1487222477894-8943e31ef7b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1326&q=80',
      cta: 'Xem thêm',
      woeid: '1252431'
    },
    {
      id: 2,
      title: 'Hà Nội',
      image: 'https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80',
      cta: 'Xem thêm',
      woeid: '1236594'
    },
    {
      id: 3,
      title: 'London',
      image: 'https://images.unsplash.com/photo-1482686115713-0fbcaced6e28?fit=crop&w=1947&q=80',
      cta: 'Xem thêm' ,
      screen: 'News',
      woeid: '44418'
    },
    {
      id: 4,
      title: 'Hồ Chí Mình(Ngày mai)',
      image: 'https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80',
      cta: 'Xem thêm',
      woeid: '1252431'
    },
    {
      id: 5,
      title: 'Hà Nội(Ngày mai)',
      image: 'https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80',
      cta: 'Xem thêm',
      woeid: '1236594'
    }
  ]
const renderArticles = (props) => {
    return(
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.articles}>
          <Block flex>
            {
                data.map(item => <CardView key={item.id} item={item} horizontal />)
            }
          </Block>
        </ScrollView>
    )
  }
const Weather = (props) => {
    const handleGoBack = () => {
        return props.navigation.goBack()
    }
    return (
        <>
        <Header 
            left 
            onPressLeft={handleGoBack} 
            title={`Xem thông tin thời tiết`} />
        <Block flex center style={styles.home}>
            {renderArticles()}
        </Block>
        </>
      
    );
  }
const styles = StyleSheet.create({
    home: {
      width: width,    
    },
    articles: {
      width: width - theme.SIZES.BASE * 2,
      paddingVertical: theme.SIZES.BASE,
    },
  });
export default Weather;