import React from 'react';
import { StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Block, theme } from 'galio-framework';

import { Card } from '../components';
import articles from '../constants/articles';
import Header from '../components/Header'
const { width } = Dimensions.get('screen');
const data = [
  {
    id: 1,
    title: 'Thời tiết',
    image: 'https://images.unsplash.com/photo-1487222477894-8943e31ef7b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1326&q=80',
    cta: 'Xem thêm',
    screen: 'Weather'
  },
  {
    id: 2,
    title: 'Đọc báo Zing',
    image: 'https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80',
    cta: 'Xem thêm',
    screen: 'News',
    url: 'https://zingnews.vn'
  },
  {
    id: 3,
    title: 'Đọc báo VnExpress',
    image: 'https://images.unsplash.com/photo-1482686115713-0fbcaced6e28?fit=crop&w=1947&q=80',
    cta: 'Xem thêm' ,
    screen: 'News',
    url: 'https://vnexpress.net'
  },
  {
    id: 4,
    title: 'Đọc truyện',
    image: 'https://images.unsplash.com/photo-1500522144261-ea64433bbe27?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2102&q=80',
    cta: 'Xem thêm',
    screen: 'News',
    url: 'https://vuighe.net'
  }
]
const renderArticles = (props) => {
  return(
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.articles}>
        <Block flex>
          {data.map((item) => <Card key={item.id} item={item} horizontal />)}
        </Block>
      </ScrollView>
  )
}

const Home = (props) => {
  const handleGoBack = () => {
    return props.navigation.goBack()
  }
  return (
    <>
    <Header 
      title={`Trang chủ`} />
    <Block flex center style={styles.home}>
      
      {renderArticles()}
    </Block>
    </>
  );
}


const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});

export default Home;
